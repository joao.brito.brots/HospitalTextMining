#' Adiciona colunas de text mining
#'
#' @param dados 
#' @param campo 
#'
#' @return
#' @export
#'
#' @examples
AdicionarTextMiningToDummyCount <- function(dados, campo){
  
  # Dados com text mining
  dadosComTextMining <- dados %>% 
    select(Id, !!campo) %>% 
    mutate(
      Grupo = ((Id %/% 30) + 1L)
    ) %>%  
    split(.$Grupo) %>% 
    lapply(
      function(dadosGrupo){
        
        # Exibe Processamento 
        Msg(campo, " ", dadosGrupo$Grupo[1])
        
        # Variaveis de text mining
        dadosGrupo %>% 
          select(Id, !!campo) %>% 
          TextMiningToDummyCount(
            campo = campo
          )
      }
    ) %>% 
    GravarCache(
      arquivo = str_c("TextMining_", campo)
    ) 

  # Retorno da funcao
  # return(dadosComTextMining)  
  invisible()
}
####
## Fim
#