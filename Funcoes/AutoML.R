#' Modelo eXtreme Gradient Boosting
#'
#' @param dadosTreino 
#' @param metodo 
#'
#' @return
#' @export
#'
#' @examples
AutoML <- function(dadosTreino, metodo){
  
  if(metodo == "nnet"){
    
    # Cria o melhor  modelo
    modelo <- caret::train(
      form = Classe ~ .,
      data = dadosTreino,
      method = if(metodo == "GMDH"){
        PolynomialNeuralNetwork
      } else {
        metodo
      },
      metric = "ROC",
      maximize = TRUE,
      # tunegrid = expand.grid(
      #   size = 2:6,
      #   decay = c(1e-3, 1e-4, 1e-5)
      # ),
      trControl = caret::trainControl(
        method = "cv",
        number = 10,
        classProbs = TRUE,
        summaryFunction = caret::multiClassSummary,
        savePredictions = TRUE,
        search = "grid",
        # selectionFunction = caret::tolerance,
        verboseIter = FALSE,
        allowParallel = TRUE
      )
    )
  } else {
    # Cria o melhor  modelo
    modelo <- caret::train(
      form = Classe ~ .,
      data = dadosTreino,
      method = if(metodo == "GMDH"){
        PolynomialNeuralNetwork
      } else {
        metodo
      },
      metric = "ROC",
      maximize = TRUE,
      trControl = caret::trainControl(
        method = "cv",
        number = 10,
        classProbs = TRUE,
        summaryFunction = caret::multiClassSummary,
        savePredictions = TRUE,
        search = "grid",
        # selectionFunction = caret::tolerance,
        verboseIter = FALSE,
        allowParallel = TRUE
      )
    )
  }
  
  # Analise do modelo
  modelo$Analise <- evalm(
    list1 = modelo,
    silent = TRUE,
    showplots = FALSE
  )
  
  # Analise do modelo
  modelo$Analise$ConfusionMatrix <- caret::confusionMatrix(
    data = modelo$pred$pred,
    reference = modelo$pred$obs,
    positive = "Sim"
  )
  
  # Retorno da funcao
  return(modelo) 
}
####
## Fim
#