#' Modelo Group Method of Data Handling
#'
#' @param dadosTreino 
#'
#' @return
#' @export
#'
#' @examples
AutoML_GMDH <- function(dadosTreino){
  
  # Cria o melhor  modelo
  modelo <- caret::train(
    form = Classe ~ .,
    data = dadosTreino,
    method = PolynomialNeuralNetwork,
    metric = "ROC",
    maximize = TRUE,
    trControl = caret::trainControl(
      method = "cv",
      number = 10,
      repeats = 2,
      classProbs = TRUE,
      summaryFunction = caret::twoClassSummary,
      savePredictions = TRUE,
      search = "grid",
      selectionFunction = caret::tolerance,
      verboseIter = FALSE,
      allowParallel = TRUE
    )
  )
  
  # Analise do modelo
  modelo$Analise <- evalm(
    modelo,
    silent = TRUE,
    showplots = FALSE
  )
  
  # Matriz de confusao
  modelo$Analise$ConfusionMatrix <- GMDH2::confMat(
    modelo, 
    positive = "Sim"
  )
  
  # Retorno da funcao
  return(modelo) 
}
####
## Fim
#