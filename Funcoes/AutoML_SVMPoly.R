#' Modelo Support Vector Machines Polinomial
#'
#' @param dadosTreino 
#'
#' @return
#' @export
#'
#' @examples
AutoML_SVMPoly <- function(dadosTreino){
  
  # Cria o melhor  modelo
  modelo <- caret::train(
    form = Classe ~ .,
    data = dadosTreino,
    method = "svmPoly",
    metric = "ROC",
    maximize = TRUE,
    trControl = caret::trainControl(
      method = "cv",
      number = 10,
      repeats = 2,
      classProbs = TRUE,
      summaryFunction = caret::twoClassSummary,
      savePredictions = TRUE,
      search = "grid",
      selectionFunction = caret::tolerance,
      verboseIter = FALSE,
      allowParallel = TRUE
    )
  )
  
  # Analise do modelo
  modelo$Analise <- evalm(
    list1 = modelo,
    silent = TRUE
  )
  
  # Matriz de confusao
  modelo$Analise$ConfusionMatrix <- caret::confusionMatrix(
    modelo, 
    positive = "Sim"
  )
  
  # Retorno da funcao
  return(modelo) 
}
####
## Fim
#