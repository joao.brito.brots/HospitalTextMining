#' Balanceamento de dados por classe CLUS
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
BalanceadorCLUS <- function(dados){
  
  # Faz balanceamento dos dados
  dadosBalanceados <- CLUS(
    x = dados %>% 
      select(-Classe) %>% 
      as.matrix(),
    y = dados$Classe,
    m = 1L,
    k = 13L
  ) %>% 
    as_tibble() %>% 
    rename(Classe = y) %>% 
    select(Classe, everything())
  
  # Retorno da funcao
  return(dadosBalanceados) 
}
####
## Fim
#
