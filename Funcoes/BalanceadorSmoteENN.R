#' Balanceamento de dados por classe Smote ENN
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
BalanceadorSmoteENN <- function(dados){
  
  # % Over
  percOver <- (dados %>% 
                 count(Classe) %>% 
                 arrange(Classe) %>% 
                 slice(1) %>% 
                 pull(n)) / nrow(dados) * 100L
  
  # Faz balanceamento dos dados
  dadosBalanceados <- SmoteENN(
    x = dados %>% 
      select(-Classe),
    y = dados$Classe,
    percOver = 200
  ) %>% 
    as_tibble() %>% 
    rename(Classe = y) %>% 
    select(Classe, everything())
  
  # Retorno da funcao
  return(dadosBalanceados) 
}
####
## Fim
#
