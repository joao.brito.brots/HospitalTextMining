#' Balanceamento de dados por classe SmoteTL
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
BalanceadorSmoteTL <- function(dados){
  
  # % Over
  percOver <- (dados %>% 
    count(Classe) %>% 
    arrange(Classe) %>% 
    slice(1) %>% 
    pull(n)) / nrow(dados) * 100L

  # Faz balanceamento dos dados
  dadosBalanceados <- SmoteTL(
    x = dados %>% 
      select(-Classe),
    y = dados$Classe,
    percOver = 600,
    k = 7
  ) %>% 
    as_tibble() %>% 
    rename(Classe = y) %>% 
    select(Classe, everything())
  
  # Retorno da funcao
  return(dadosBalanceados) 
}
####
## Fim
#
