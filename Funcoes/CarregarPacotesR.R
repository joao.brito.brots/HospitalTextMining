#' Carregar Pacotes R
#'
#' @param pacotes 
#'
#' @return
#' @export
#'
#' @examples
CarregarPacotesR <- function(pacotes) {
  
  # Pacotes R
  for(pacote in pacotes){
    
    print(pacote)
    
    # Se nao possui o pacote instalado
    if(!require(pacote, character.only = TRUE, quietly = TRUE, warn.conflicts = FALSE)){
      
      # Pacote PTtextmining
      if(pacote == "PTtextmining"){
        devtools::install_github("dfalbel/PTtextmining")
        require(PTtextmining)
        next()
      }
      
      # Pacote ptstem
      if(pacote == "ptstem"){
        devtools::install_github("dfalbel/ptstem")
        require(ptstem)
        next()
      }
      
      # Pacote multidplyr
      if(pacote == "multidplyr"){
        devtools::install_github("tidyverse/multidplyr")
        require(multidplyr)
        next()
      }
      
      # Instala pacote
      install.packages(
        pkgs = pacote,
        dependencies = TRUE,
        type = "binary",
        clean = TRUE,
        Ncpus = parallel::detectCores()
      )
      
      # Carrega pacote
      require(
        package = pacote, 
        character.only = TRUE, 
        quietly = TRUE, 
        warn.conflicts = FALSE
      )
    }
  }
}
####
## Fim
#