#' Carrega parametros do sistema
#'
#' @return
#' @export
#'
#' @examples
CarregarParSys <- function(){
  
  # Carga inicial dos parametros no Config.ini
  parSys <- CarregarConfig.ini()
  
  # Retorno da funcao
  return(parSys)
}
####
## Fim
#