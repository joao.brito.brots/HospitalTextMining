#' Title
#'
#' @param dadosTreino 
#'
#' @return
#' @export
#'
#' @examples
ClassificadorGBM <- function(dadosTreino){
 
  # Cria modelo
  modelo <- gbm(
    formula = Classe ~ .,
    data = dadosTreino %>% 
      mutate(
        Classe = Classe %>% 
          as.logical() %>% 
          as.numeric()
      ),
    distribution = "bernoulli",
    cv.folds = 50L,
    verbose = FALSE
  )
  
  # Retorno da funcao
  return(modelo) 
}
####
## Fim
#