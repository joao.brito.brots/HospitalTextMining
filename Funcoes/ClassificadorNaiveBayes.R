#' Cria modelo Naive Bayes
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
ClassificadorNaiveBayes <- function(dados){
  
  # Cria modelo preditivo
  modelo <- naiveBayes(
    formula = Classe ~ .,
    data = dados
  )
  
  # Retorno da funcao
  return(modelo) 
}
####
## Fim
#
