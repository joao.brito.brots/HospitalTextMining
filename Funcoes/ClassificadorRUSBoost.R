#' Classificador RUSBoost
#'
#' @param dadosTreino 
#'
#' @return
#' @export
#'
#' @examples
ClassificadorRUSBoost <- function(dadosTreino){
  
  # Cria modelo
  modelo <- bboost(
    x = dadosTreino %>% 
      select(-Classe) %>% 
      mutate_if(is.logical, LogicalToFactorLogical),
    y = dadosTreino$Classe,
    type = "RUSBoost"
  )
  
  # Retorno da funcao
  return(modelo)
}
####
## Fim
#
