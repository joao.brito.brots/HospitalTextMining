#' Coeficiente variacao
#'
#' @param vetor 
#'
#' @return
#' @export
#'
#' @examples
CoeficienteVariacao <- function(vetor){
  
  # Calcula 
  coeficienteVariacao <- (sd(vetor, na.rm = TRUE) / mean(vetor, na.rm = TRUE))

  # Retorno da funcao
  return(coeficienteVariacao) 
}
####
## Fim
#