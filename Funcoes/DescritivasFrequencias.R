#' Frequencias dos valores
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
DescritivasFrequencias <- function(dados){

  # Ajusta para quantificacao  
  dados %<>% 
    mutate_all(as.character)
  
  frequencias <- dados %>% 
    colnames() %>% 
    lapply(
      FUN = function(nomeColuna){
        
        print(nomeColuna)
        
        try(
          expr = dados %>% 
            rename(
              Conteudo = all_of(nomeColuna)
            ) %>% 
            count(Conteudo, sort = TRUE) %>% 
            PercAcum() %>% 
            mutate(
              Campo = nomeColuna
            ) %>% 
            select(Campo, everything()),
          silent = FALSE
        )
        
      }
    ) %>% 
    bind_rows()
  
  # Retorno da funcao
  return(frequencias)
}
####
## Fim
#