#' Verifica se deu erro
#'
#' @return
#' @export
#'
#' @examples
Erro <- function(param){
  any(class(param) == "try-error")
}
####
## Fim
#