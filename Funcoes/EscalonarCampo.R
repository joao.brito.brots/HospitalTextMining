#' Escalonar Campo
#'
#' @param campo 
#'
#' @return
#' @export
#'
#' @examples
EscalonarCampo <- function(campo, escalaInicial, escalaFinal){
  
  # Calcula numerador
  campoEscalonado <- (((escalaFinal - escalaInicial) / (max(campo, na.rm = TRUE) - min(campo, na.rm = TRUE))) * (campo - max(campo, na.rm = TRUE))) + escalaFinal
  
  # Retorno da funcao
  return(campoEscalonado)
}
####
## Fim
#