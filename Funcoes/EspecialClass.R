#' Testa se e da classe especial
#'
#' @param vetor 
#' @param especialClass 
#'
#' @return
#' @export
#'
#' @examples
EspecialClass <- function(vetor, especialClass){
  
  # Apura a classe especial
  classeEspecial <- attr(vetor, "EspecialClass")
  
  if(is.null(classeEspecial)){
    classeEspecial <- "NAO"
  }
  
  # Apura se e de classe especial
  resultado <- (classeEspecial == especialClass)
  
  # Retorno da funcao
  return(resultado)
}
####
## Fim
#