#' Reestrutura dados com campos preditores
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
FeatureEngineering <- function(dados, buscarCache = FALSE, arquivoCache){
  
  # Cache Reetruturacao dos dados
  arquivoCache <- str_c("DadosReestruturados_", arquivoCache)
  
  # Se possui cache
  if(buscarCache & ExisteCache(arquivoCache)){
    
    # Carrega dados cache
    dadosReestruturados <- BuscarCache(
      arquivo = arquivoCache
    ) 
    
    # Retorno da funcao
    return(dadosReestruturados)
  }
  
  # Monta dados para predicao
  dadosReestruturados <- dados %>% 
    mutate(
      Id = row_number(),
      Idade = ((DT_NOTIFIC - DT_NASC) / 365) %>% 
        as.double(),
      CicloVital = str_c("CicloVital_", CicloVital(Idade)),
      AnoPriSintomas = year(DT_SIN_PRI),
      CS_SEXO = str_c("Sexo_", CS_SEXO)
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "CicloVital"
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "CS_SEXO",
      excluirDummy = "Sexo_I"
    ) %>% 
    mutate(
      Gestante = (CS_GESTANT %in% 1:4),
      CS_GESTANT = NULL
    ) %>% 
    mutate(
      Febre = case_when(
        (FEBRE == 1) ~ "Febre_Sim", 
        (FEBRE == 2) ~ "Febre_Nao",
        TRUE         ~ "Febre_Desconhecido"   
      ),
      FEBRE = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Febre",
      excluirDummy = "Febre_Desconhecido"
    ) %>% 
    mutate(
      Tosse = case_when(
        (TOSSE == 1) ~ "Tosse_Sim", 
        (TOSSE == 2) ~ "Tosse_Nao",
        TRUE         ~ "Tosse_Desconhecido"   
      ),
      TOSSE = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Tosse",
      excluirDummy = "Tosse_Desconhecido"
    ) %>% 
    mutate(
      Garganta = case_when(
        (GARGANTA == 1) ~ "Garganta_Sim", 
        (GARGANTA == 2) ~ "Garganta_Nao",
        TRUE            ~ "Garganta_Desconhecido"   
      ),
      GARGANTA = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Garganta",
      excluirDummy = "Garganta_Desconhecido"
    ) %>% 
    mutate(
      Dispneia = case_when(
        (DISPNEIA == 1) ~ "Dispneia_Sim", 
        (DISPNEIA == 2) ~ "Dispneia_Nao",
        TRUE            ~ "Dispneia_Desconhecido"   
      ),
      DISPNEIA = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Dispneia",
      excluirDummy = "Dispneia_Desconhecido"
    ) %>% 
    mutate(
      DescofResp = case_when(
        (DESC_RESP == 1) ~ "DescofResp_Sim", 
        (DESC_RESP == 2) ~ "DescofResp_Nao",
        TRUE             ~ "DescofResp_Desconhecido"   
      ),
      DESC_RESP = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "DescofResp",
      excluirDummy = "DescofResp_Desconhecido"
    ) %>% 
    mutate(
      Saturacao = case_when(
        (SATURACAO == 1) ~ "Saturacao_Sim", 
        (SATURACAO == 2) ~ "Saturacao_Nao",
        TRUE             ~ "Saturacao_Desconhecido"   
      ),
      SATURACAO = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Saturacao",
      excluirDummy = "Saturacao_Desconhecido"
    ) %>% 
    mutate(
      Diarreia = case_when(
        (DIARREIA == 1) ~ "Diarreia_Sim", 
        (DIARREIA == 2) ~ "Diarreia_Nao",
        TRUE            ~ "Diarreia_Desconhecido"   
      ),
      DIARREIA = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Diarreia",
      excluirDummy = "Diarreia_Desconhecido"
    ) %>% 
    mutate(
      Vomito = case_when(
        (VOMITO == 1) ~ "Vomito_Sim", 
        (VOMITO == 2) ~ "Vomito_Nao",
        TRUE          ~ "Vomito_Desconhecido"   
      ),
      VOMITO = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Vomito",
      excluirDummy = "Vomito_Desconhecido"
    ) %>% 
    mutate(
      Puerpera = case_when(
        (PUERPERA == 1) ~ "Puerpera_Sim", 
        (PUERPERA == 2) ~ "Puerpera_Nao",
        TRUE            ~ "Puerpera_Desconhecido"   
      ),
      PUERPERA = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Puerpera",
      excluirDummy = "Puerpera_Desconhecido"
    ) %>% 
    mutate(
      Cardiopati = case_when(
        (CARDIOPATI == 1) ~ "Cardiopati_Sim", 
        (CARDIOPATI == 2) ~ "Cardiopati_Nao",
        TRUE              ~ "Cardiopati_Desconhecido"   
      ),
      CARDIOPATI = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Cardiopati",
      excluirDummy = "Cardiopati_Desconhecido"
    ) %>% 
    mutate(
      Hematologi = case_when(
        (HEMATOLOGI == 1) ~ "Hematologi_Sim", 
        (HEMATOLOGI == 2) ~ "Hematologi_Nao",
        TRUE              ~ "Hematologi_Desconhecido"   
      ),
      HEMATOLOGI = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Hematologi",
      excluirDummy = "Hematologi_Desconhecido"
    ) %>% 
    mutate(
      SindDown = case_when(
        (SIND_DOWN == 1) ~ "SindDown_Sim", 
        (SIND_DOWN == 2) ~ "SindDown_Nao",
        TRUE             ~ "SindDown_Desconhecido"   
      ),
      SIND_DOWN = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "SindDown",
      excluirDummy = "SindDown_Desconhecido"
    ) %>% 
    mutate(
      Hepatica = case_when(
        (HEPATICA == 1) ~ "Hepatica_Sim", 
        (HEPATICA == 2) ~ "Hepatica_Nao",
        TRUE            ~ "Hepatica_Desconhecido"   
      ),
      HEPATICA = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Hepatica",
      excluirDummy = "Hepatica_Desconhecido"
    ) %>% 
    mutate(
      Asma = case_when(
        (ASMA == 1) ~ "Asma_Sim", 
        (ASMA == 2) ~ "Asma_Nao",
        TRUE        ~ "Asma_Desconhecido"   
      ),
      ASMA = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Asma",
      excluirDummy = "Asma_Desconhecido"
    ) %>% 
    mutate(
      Diabetes = case_when(
        (DIABETES == 1) ~ "Diabetes_Sim", 
        (DIABETES == 2) ~ "Diabetes_Nao",
        TRUE            ~ "Diabetes_Desconhecido"   
      ),
      DIABETES = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Diabetes",
      excluirDummy = "Diabetes_Desconhecido"
    ) %>% 
    mutate(
      Neurologic = case_when(
        (NEUROLOGIC == 1) ~ "Neurologic_Sim", 
        (NEUROLOGIC == 2) ~ "Neurologic_Nao",
        TRUE              ~ "Neurologic_Desconhecido"   
      ),
      NEUROLOGIC = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Neurologic",
      excluirDummy = "Neurologic_Desconhecido"
    ) %>% 
    mutate(
      Pneumopati = case_when(
        (PNEUMOPATI == 1) ~ "Pneumopati_Sim", 
        (PNEUMOPATI == 2) ~ "Pneumopati_Nao",
        TRUE              ~ "Pneumopati_Desconhecido"   
      ),
      PNEUMOPATI = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Pneumopati",
      excluirDummy = "Pneumopati_Desconhecido"
    ) %>% 
    mutate(
      Imunodepre = case_when(
        (IMUNODEPRE == 1) ~ "Imunodepre_Sim", 
        (IMUNODEPRE == 2) ~ "Imunodepre_Nao",
        TRUE              ~ "Imunodepre_Desconhecido"   
      ),
      IMUNODEPRE = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Imunodepre",
      excluirDummy = "Imunodepre_Desconhecido"
    ) %>% 
    mutate(
      Renal = case_when(
        (RENAL == 1) ~ "Renal_Sim", 
        (RENAL == 2) ~ "Renal_Nao",
        TRUE         ~ "Renal_Desconhecido"   
      ),
      RENAL = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Renal",
      excluirDummy = "Renal_Desconhecido"
    ) %>% 
    mutate(
      Obesidade = case_when(
        (OBESIDADE == 1) ~ "Obesidade_Sim", 
        (OBESIDADE == 2) ~ "Obesidade_Nao",
        TRUE             ~ "Obesidade_Desconhecido"   
      ),
      OBESIDADE = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "Obesidade",
      excluirDummy = "Obesidade_Desconhecido"
    ) %>% 
    mutate(
      DiasEntreVacinaHospital = (DT_INTERNA - DT_UT_DOSE) %>% 
        as.double() %>%  # Nao precisa decimais, dias inteiros
        coalesce(365 * 10), # Preenche os NA's 10 anos em dias
      MaeVacinada = (MAE_VAC == 1),
      DiasEntreVacinaMaeHospital = (DT_INTERNA - DT_VAC_MAE) %>% 
        as.double() %>%  # Nao precisa decimais, dias inteiros
        coalesce(365 * 10), # Preenche os NA's 10 anos em dias
      RAIOX_RES = RAIOX_RES %>% 
        as.character() %>% 
        coalesce("0"),
      RaioX_Resultado = str_c("RaioX_Resultado_", RAIOX_RES),
      RAIOX_RES = NULL,
      PCR_OutroMetodoBioMolec = case_when(
        (PCR_RESUL == 1) ~ "PCR_OutroMetodoBioMolec_Detectavel", 
        (PCR_RESUL == 2) ~ "PCR_OutroMetodoBioMolec_NaoDetectavel",
        (PCR_RESUL == 3) ~ "PCR_OutroMetodoBioMolec_Inconclusivo",
        (PCR_RESUL == 4) ~ "PCR_OutroMetodoBioMolec_NaoRealizado",
        TRUE             ~ "PCR_OutroMetodoBioMolec_Ignorado"   
      ),
      PCR_RESUL = NULL,
      PCR_Influenza = case_when(
        (POS_PCRFLU == 1) ~ "PCR_Influenza_Sim", 
        (POS_PCRFLU == 2) ~ "PCR_Influenza_Nao",
        TRUE              ~ "PCR_Influenza_Ignorado"   
      ),
      POS_PCRFLU = NULL,
      PCR_OutrosVirus = case_when(
        (POS_PCROUT == 1) ~ "PCR_OutrosVirus_Sim", 
        (POS_PCROUT == 2) ~ "PCR_OutrosVirus_Nao",
        TRUE              ~ "PCR_OutrosVirus_Ignorado"   
      ),
      POS_PCROUT = NULL,
      SuporteVen = case_when(
        (SUPORT_VEN == 1) ~ "SuporteVen_Invasivo", 
        (SUPORT_VEN == 2) ~ "SuporteVen_NaoInvasivo",
        (SUPORT_VEN == 3) ~ "SuporteVen_NaoUtilizou",
        TRUE              ~ "SuporteVen_Ignorado"   
      ),
      SUPORT_VEN = NULL
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "SuporteVen",
      excluirDummy = "SuporteVen_Ignorado"
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "RaioX_Resultado",
      excluirDummy = "RaioX_Resultado_0"
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "PCR_OutroMetodoBioMolec",
      excluirDummy = "PCR_OutroMetodoBioMolec_Ignorado"
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "PCR_Influenza",
      excluirDummy = "PCR_Influenza_Ignorado"
    ) %>% 
    FactorToDummy(
      chave = "Id", 
      campo = "PCR_OutrosVirus",
      excluirDummy = "PCR_OutrosVirus_Ignorado"
    )
  
  # Seleciona somente as novas colunas
  dadosReestruturados %<>% 
    select(Classe, Idade:last_col(), OUTRO_DES, MORB_DESC) %>% 
    EliminarVariaveisRedundantes() 
  
  # Ordena as colunas
  dadosReestruturados %<>% 
    select(Classe, !!sort(colnames(dadosReestruturados)))
  
  # Ajustes complementares
  dadosReestruturados %<>% 
    mutate_if(is.double, ScaleNormalizar) %>% 
    mutate_if(is.logical, as.numeric) %>% 
    AdicionarVariaveisModeradoras()
  
  # Mineracao Texto
  dadosReestruturados %<>% 
    TextMiningToDummy(
      campo = "OUTRO_DES"
    ) %>% 
    TextMiningToDummy(
      campo = "MORB_DESC"
    ) 
  
  # Elimina variaveis com redundante
  dadosReestruturados %<>% 
    EliminarVariaveisRedundantes()
  
  # Grava dados
  GravarCache(
    dados = dadosReestruturados %>% 
      mutate(
        Classe = Classe %>% 
          as.logical() %>% 
          factor(
            levels = c("FALSE", "TRUE")
          )
      ),
    arquivo = arquivoCache
  )
  
  # Retorno da funcao
  return(dadosReestruturados) 
}
####
## Fim
#