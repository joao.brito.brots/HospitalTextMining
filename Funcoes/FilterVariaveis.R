#' Filter de variaveis
#'
#' @param dados 
#' @param tolDoMaisFreq 
#'
#' @return
#' @export
#'
#' @examples
FilterVariaveis <- function(dados){
  
  # Eliminacao basica
  dados %<>% 
    EliminarVariaveisRedundantes()
  
  # Vetor de variaveis 
  variaveis <- character()
  
  # Variaveis
  for(coluna in colnames(dados)[-1]){
    
    # Dados de teste
    dadosTeste <- dados %>% 
      select(Classe, !!coluna)
    
    # Teste conteudo unico
    teste <- dadosTeste %>% 
      pull(!!coluna) %>% 
      unique() %>% 
      length()
    
    # Se possui somente um conteudo
    if(teste < 2){
      next()
    }
    
    # Monta formula 
    formula <- str_c("Classe ~ ", coluna) %>% 
      formula()
    
    # Se forem duas categorias
    if(teste == 2){
      # Teste conteudo unico Mann-Whitney U
      teste <- wilcox.test(
        formula = formula,
        data = dados %>% 
          select(Classe, !!coluna) %>% 
          mutate_all(FactorLogicalToLogical) %>% 
          mutate_all(as.numeric),
        paired = FALSE,
        exact = TRUE
      )
    } else {
      # Teste conteudo unico Kruskal-Wallis Rank Sum Test
      teste <- kruskal.test(
        formula = formula,
        data = dados %>% 
          select(Classe, !!coluna)
      )
    }
    
    # Se nao for significativa
    if(teste$p.value > 0.05){
      next()
    } 
    
    # Adiciona variaveis
    variaveis <- c(variaveis, coluna)
  }
  
  # Retorno da funcao
  return(dados)
}
####
## Fim
#
