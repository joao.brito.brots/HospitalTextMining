#' Carrega resultados do simulador
#'
#' @return
#' @export
#'
#' @examples
LoadResultadosSimulador <- function(){
  
  # Carrega arquivo
  arquivoRdata <-"Dados/ResultadosSimulador.Rdata"
  
  # Se possui resultados anteriores
  if(file.exists(arquivoRdata)){
    load(arquivoRdata)
  } else {
    resultadosSimulador <-as_tibble() 
  }
  
  # Retorno da funcao
  return(resultadosSimulador) 
}
####
## Fim
#