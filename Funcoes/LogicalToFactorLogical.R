#' Converte campo logical em factor logical
#'
#' @param vetor 
#'
#' @return
#' @export
#'
#' @examples
LogicalToFactorLogical <- function(vetor){
  
  # Se for campo logical 
  if(is.logical(vetor)){
    
    # Converte para factor
    vetor %<>% 
      as.integer() %>% 
      factor(
        ordered = TRUE,
        levels = c(0L:1L)
      ) %>% 
      fct_rev()
    
    # Classe fator, ordered. Logical 
    attr(vetor, "EspecialClass") <- "FactorLogical"
  }

  # Retorno da funcao  
  return(vetor)
}
####
## Fim
#