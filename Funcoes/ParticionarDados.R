#' Particionar dados
#'
#' @param dados 
#' @param balanceador 
#'
#' @return
#' @export
#'
#' @examples
ParticionarDados <- function(dados, balanceador){
  
  # Particionamento dos dados em Calibracao e Validacao
  dadosSimulacao <- ParticionarTreinoTeste(
    dados = dados,
    fracaoTreino = parSys$Geral$PorcaoTreino,
    nomeTreino = "Calibracao",
    nomeTeste = "Validacao"
  )

  # Iguala colunas para validacao
  dadosSimulacao$Validacao %<>% 
    select(!!colnames(dadosSimulacao$Calibracao))
  
  # Processa balanceamento
  for(i in 1L:5L){
    
    # Balanceamento de registros por classe
    dadosSimulacaoBalanceados <- ChamarFuncao(
      dados = dadosSimulacao$Calibracao,
      funcao = balanceador
    )
    
    # Se NAO houve erro com o balanceador
    if(!Erro(dadosSimulacaoBalanceados)){
      break()
    }
  }
  
  # Se houve erro com o balanceador
  if(Erro(dadosSimulacaoBalanceados)){
    return(dadosSimulacaoBalanceados)
  }
  
  # Objeto com fracoes dos dados
  dadosParticionados <- list(
    CalibracaoBalanceado = dadosSimulacaoBalanceados,
    Validacao = dadosSimulacao$Validacao
  )
  
  # Retorna particionamento
  return(dadosParticionados)
}
####
## Fim
#