#' Particionamento dos dados em treino e teste
#'
#' @param dados 
#' @param fracaoTreino 
#' @param nomeTreino 
#' @param nomeTeste 
#'
#' @return
#' @export
#'
#' @examples
ParticionarTreinoTeste <- function(dados, fracaoTreino = parSys$Geral$PorcaoTreino, nomeTreino = "Treino", nomeTeste = "Teste"){

  # Objeto para armazenar particoes
  dadosParticionados <- list()

  # Adiciona Id
  dados %<>% 
    mutate(
      Id = row_number()
    ) %>% 
    select(Id, everything())
  
  # Forca aleatorizacao
  set.seed(Sys.time())
  
  # Particionamento dados treino
  dadosParticionados[[nomeTreino]] <- dados %>% 
    group_by(Classe) %>% 
    sample_frac(fracaoTreino) %>% 
    ungroup()

  # Particionamento dados teste
  dadosParticionados[[nomeTeste]] <- dados %>% 
    anti_join(
      dadosParticionados[[nomeTreino]],
      by = "Id"
    ) %>% 
    select(Id, everything())
  
  # Particionamento dados treino
  dadosParticionados[[nomeTreino]] %<>% 
    select(Id, everything())

  # Liberar Memoriaq
  gc(reset = TRUE, full = TRUE)
  
  # Retorno da funcao
  return(dadosParticionados) 
}
####
## Fim
#