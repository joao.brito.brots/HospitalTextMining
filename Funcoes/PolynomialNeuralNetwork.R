# Modelo Arima
PolynomialNeuralNetwork <- list(
  type = "Classification",
  library = "GMDH2",
  loop = NULL,
  parameters = data.frame(
    parameter = "hp_alpha",
    class = "double",
    label = "Alpha"
  ),
  grid = function(x, y, len = NULL, search = "grid"){
    expand.grid(
      hp_alpha = seq(
        from = 0.1,
        to = 0.9,
        by = 0.02
      )
    )
  },
  fit = function(x, y, wts, parameter, lev, last, weights, classProbs, ...) { 
    ClassificadorGMDH(
      dados = x %>% 
        as_tibble() %>% 
        mutate(
          Classe = y
        ),
      hp_alpha = parameter$hp_alpha
    )
  },
  predict = function(modelFit, newdata, preProc = NULL, submodels = NULL){
    predict(
      modelFit,
      x = newdata %>% 
        as.matrix(),
      type = "class"
    )
  },
  prob = function(modelFit, newdata, preProc = NULL, submodels = NULL){
    predict(
      modelFit,
      x = newdata %>% 
        as.matrix(),
      type = "probability"
    )
  },
  sort = function(x){
    x
  },
  levels = function(x){
    c("Sim", "Nao")
  }
)
####
## Fim
#