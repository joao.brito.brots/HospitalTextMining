#' Predicao Geral
#'
#' @param modelo 
#' @param dadosTeste 
#'
#' @return
#' @export
#'
#' @examples
PredictGeral <- function(dadosTeste, modelo){
  
  # Predicao
  predicao <- predict(
    object = modelo, 
    newdata = dadosTeste %>% 
      select(-Classe) %>% 
      as.data.frame(),
    type = "prob"
  ) %>% 
    as_tibble() %>% 
    pull(`TRUE`) %>% 
    as.double() %>% 
    round(0) %>% 
    as.logical() %>% 
    factor(
      levels = c("FALSE", "TRUE")
    )
  
  # Retorno da funcao
  return(predicao)
}
####
## Fim
#