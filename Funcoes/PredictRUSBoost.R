#' Predicao RUSBoost
#'
#' @param modelo 
#' @param dadosTeste 
#'
#' @return
#' @export
#'
#' @examples
PredictRUSBoost <- function(dadosTeste, modelo){
  
  # Predicao
  predicao <- predict(
    modelo, 
    dadosTeste %>% 
      select(-Classe) %>% 
      as.data.frame()
  ) %>% 
    factor(
      levels = c("FALSE", "TRUE")
    )
  
  # Retorno da funcao
  return(predicao)
}
####
## Fim
#