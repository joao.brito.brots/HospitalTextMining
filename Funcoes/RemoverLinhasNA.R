#' Remover Linhas com NA
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
RemoverLinhasNA <- function(dados){
  
  # Remove linhas com NA
  dados %<>% 
    mutate(
      Linha = row_number(),
    ) %>% 
    anti_join(
      dados %>% 
        select(-Classe) %>% 
        transmute(
          Linha = row_number(),
          PossuiNA = is.na(rowSums(.))
        ) %>% 
        filter(PossuiNA),
      by = "Linha"
    ) %>% 
    select(-Linha)
  
  # Retorno dos dados
  return(dados)
}
####
## Fim
#