#' Remover stop words
#'
#' @param palavras 
#'
#' @return
#' @export
#'
#' @examples
RemoverStopWords <- function(palavras){

  # Remover stop words
  palavras <- str_replace_all(
    string = palavras,
    pattern = str_c(
      "<|>|=|[:digit:]|[:punct:]|([[:alpha:]])\\1+ | ",
      str_c(
        BuscarStopWords(),
        collapse = " | "
      )
    ),
    replacement = " "
  ) %>%
    TratarAlfanumericos()
  
  # Retorno da funcao
  return(palavras)
}
####
## Fim
#