#' Salva resultados do simulador
#'
#' @param resultadosSimulador 
#'
#' @return
#' @export
#'
#' @examples
SaveResultadosSimulador <- function(resultadosSimulador){
  
  # Salva Resultados 
  write_rds(
    resultadosSimulador,
    path = "Resultados/ResultadosSimulador.RDS"
  )
  
  # Funcao sem retorno
  invisible()
}
####
## Fim
#