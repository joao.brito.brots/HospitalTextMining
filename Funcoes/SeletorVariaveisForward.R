#' Selecao de variaveis Forward 
#'
#' @param dados 
#' @param balanceador 
#'
#' @return
#' @export
#'
#' @examples
SeletorVariaveisForward <- function(dados){
  
  # Gera ranking de variaveis
  rankingVariaveis <- ChamarFuncao(
    dados = dados,
    funcao = parSys$SeletorVariaveis$Ranking
  )
  
  # Inicializa selecionadas
  varSelecionadas <- character()
  
  # Resultados a cada adicao de variavel
  resultadosIteracao <- tibble()

  # Numero da variavel
  numVar <- 0L
  qtdVar <- length(rankingVariaveis$Variavel)
  varCandidata <- rankingVariaveis$Variavel[1]
  # Processo iterativo de adicao de variaveis
  for(varCandidata in rankingVariaveis$Variavel){
    
    # Incrementa variavel
    numVar <- numVar + 1L
    
    # Adiciona candidata
    varSelecionadas <- c(varSelecionadas, varCandidata)
    
    # Cria modelo preditivo
    modelo <- ChamarFuncao(
      dados = dados %>% 
        select(Classe, !!varSelecionadas),
      funcao = parSys$SeletorVariaveis$Classificador
    )
    
    # Se houve erro 
    if(Erro(modelo)){
      next()
    }
    
    # Matriz de confusao
    matrizConfusao <- VerificarAcuracia(
      dadosTeste = dados %>% 
        select(Classe, !!varSelecionadas),
      modelo = modelo,
      classificador = parSys$SeletorVariaveis$Classificador
    )
    
    # Se houve erro 
    if(Erro(matrizConfusao)){
      next()
    }
    
    # Exibe processamento
    Msg(
      bold(yellow("Selecao variaveis Forward...")),
      green("\nCandidata: "), numVar, "/", qtdVar, " - ", varCandidata, 
      "\nCRV: ", matrizConfusao$CRV,
      "\nAcuracia: ", matrizConfusao$overall[1],
      "\nSensibilidade: ", matrizConfusao$byClass[1],
      "\nEspecificidade: ", matrizConfusao$byClass[2]
    )
    
    # Resultados de cada iteracao
    resultadosIteracao %<>% 
      bind_rows(
        tibble(
          Variavel = varCandidata,
          CRV = matrizConfusao$CRV
        )
      )
    
    # Se finalizou
    if(matrizConfusao$CRV == 1){
      # Finaliza processo
      break()
    }
  }
  
  # Adiciona id
  resultadosIteracao %<>% 
    mutate(
      Linha = row_number()
    )
  
  # Variaveis selecionada
  varSelecionadas <- resultadosIteracao %>% 
    filter(
      Linha <= (
        resultadosIteracao %>% 
          mutate(
            CRV = round(
              x = CRV, 
              digits = 3L
            )
          ) %>% 
          filter(
            (CRV == max(CRV))
          ) %>% 
          dplyr::slice(1) %>% 
          pull(Linha)
      )
    ) %>% 
    pull(Variavel)
  
  # Retorno da funcao
  return(varSelecionadas)
}
####
## Fim
#
