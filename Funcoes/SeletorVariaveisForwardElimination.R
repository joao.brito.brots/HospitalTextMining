#' Selecao de variaveis Forward Elimination
#'
#' @param dados 
#' @param balanceador 
#'
#' @return
#' @export
#'
#' @examples
SeletorVariaveisForwardElimination <- function(dados){
  
  # Particionar dados
  dadosParticionados <- dados %>% 
    ParticionarTreinoTeste()

  # Gera ranking de variaveis
  rankingVariaveis <- ChamarFuncao(
    dados = dadosParticionados$Treino,
    funcao = parSys$SeletorVariaveis$Ranking
  )
  
  # Inicializa selecionadas
  varSelecionadas <- character()
  
  # Cria objeto da matriz atual
  matrizConfusaoAtual <- NA
  
  varCandidata <-  rankingVariaveis$Variavel[1]
  
  # Processo iterativo de adicao de variaveis
  for(varCandidata in rankingVariaveis$Variavel){
    
    # Exibe processamento
    Msg(
      bold(yellow("Selecao variaveis Forward Elimination...")),
      bold(green("\nCandidata: ")), bold(green(varCandidata))
    )
    
    # Adiciona candidata
    varSelecionadas <- c(varSelecionadas, varCandidata)
    
    # Cria modelo preditivo
    modelo <- ChamarFuncao(
      dados = dadosParticionados$Treino %>% 
        select(Classe, !!varSelecionadas),
      funcao = parSys$SeletorVariaveis$Classificador
    )
    
    # Se houve erro 
    if(Erro(modelo)){
      next()
    }
    
    # Matriz de confusao
    matrizConfusaoNova <- VerificarAcuracia(
      dadosTeste = dadosParticionados$Teste %>% 
        select(Classe, !!varSelecionadas),
      modelo = modelo,
      classificador = parSys$SeletorVariaveis$Classificador
    )
    
    # Se houve erro 
    if(Erro(matrizConfusaoNova)){
      next()
    }
    
    # Exibe processamento
    Msg(
      "  Acuracia: ", round(matrizConfusaoNova$overall[1], 2)
    )  
    
    # Se for a primeira variavel
    if(is.na(matrizConfusaoAtual)){
      
      # Atualiza matriz de confusao
      matrizConfusaoAtual <- matrizConfusaoNova
      
      # Proxima iteracao
      next()
    }
    
    # Se deve reter a variavel
    if(matrizConfusaoNova$CRV > matrizConfusaoAtual$CRV){
      
      # Atualiza matriz de confusao
      matrizConfusaoAtual <- matrizConfusaoNova
      
      # Se finalizou
      if(matrizConfusaoAtual$CRV == 1){
        # Finaliza processo
        break()
      }
      
      # Proxima iteracao
      next()
    }
    
    # Inicializa com a primeira variavel
    varSelecionadas <- varSelecionadas[varSelecionadas != varCandidata]
  }
  
  # Retorno da funcao
  return(varSelecionadas)
}
####
## Fim
#
