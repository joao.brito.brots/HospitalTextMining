#' Selecao de variaveis Nenhum
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
SeletorVariaveisNenhum <- function(dados){

  # Inicializa selecionadas
  varSelecionadas <- dados %>% 
    select(-Classe) %>% 
    colnames()

  # Retorno da funcao
  return(varSelecionadas)
}
####
## Fim
#
