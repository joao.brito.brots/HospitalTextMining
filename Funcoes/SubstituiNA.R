#' Substituir NA
#'
#' @param campo 
#'
#' @return
#' @export
#'
#' @examples
SubstituiNA <- function(campo) {
 
  # Retira os NAs
  campoSemNA <- na.omit(campo)
  
  if(length(campoSemNA) > 0){
    campo <- coalesce(campo, rev(campoSemNA)[1])
  }
  
  # Retorno dos dados
  return(campo) 
}
####
## Fim
#