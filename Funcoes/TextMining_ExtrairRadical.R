#' Extrai radical das palavras
#'
#' @param palavras 
#'
#' @return
#' @export
#'
#' @examples
TextMining_ExtrairRadical <- function(palavras){
  
  # Carrega e in
  CarregarPacotesR("ptstem")
  
  # Indica processamento
  Msg(cyan(bold("TextMining_ExtrairRadical")))
  
  # Extrai o radical das palavras
  radicalPalavras <- ptstem(
    texts = palavras, 
    algorithm = "porter", 
    complete = FALSE
  ) 
  
  # Retorno da funcao
  return(radicalPalavras)
}
####
## Fim
#