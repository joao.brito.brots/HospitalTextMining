#' Sugere correcao de palavras
#'
#' @param palavras 
#' @param correta 
#'
#' @return
#' @export
#'
#' @examples
TextMining_SugerirCorrecao <- function(palavras, correta){
  
  # Indica processamento
  Msg(cyan(bold("TextMining_SugerirCorrecao")))
  
  # Tabela de trocas
  tabela <- tibble(
    Palavras = palavras,
    Correta = correta
  ) %>% 
    mutate(
      Linha = row_number() 
    ) %>% 
    select(Linha, everything())
  
  # Corrige palavras erradas
  corrigidas <- hunspell_suggest(
    words = tabela %>% 
      filter(!Correta) %>% 
      pull(Palavras),
    dict = dictionary(
      lang = "pt_BR",
      cache = TRUE
    )
  ) %>% 
    lapply(
      FUN = function(sugestoes){
        sugestoes[1] %>% 
          TratarAlfanumericos() %>% 
          str_replace_all(" ", "")
      }
    ) %>% 
    unlist()
  
  # Sugestoes de palavras
  sugestoes <- tabela %>% 
    left_join(
      tabela %>% 
        filter(!Correta) %>% 
        mutate(
          Corrigidas = corrigidas
        ) %>% 
        select(Linha, Corrigidas),
      by = "Linha"
    ) %>% 
    mutate(
      Palavras = case_when(
        is.na(Corrigidas)     ~ Palavras,
        (Palavras == "CONSC") ~ "CONSCIENCIA",
        TRUE                  ~ Corrigidas
      )
    ) %>% 
    pull(Palavras)

  # Retorno da funcao
  return(sugestoes) 
}
####
## Fim
#