#' Mineracao de texto transforma para minusculo
#'
#' @param vetorTexto 
#'
#' @return
#' @export
#'
#' @examples
TextMining_TranformarMinisculo <- function(vetorTexto){
 
  # Executa transformacao
  vetorTexto %<>% 
    str_to_lower()
  
  # Retorno da funcao
  return(vetorTexto) 
}
####
## Fim
#