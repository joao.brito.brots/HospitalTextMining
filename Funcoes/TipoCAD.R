#' Tipo CAD
#'
#' @return
#' @export
#'
#' @examples
TipoCAD <- function(){
  str_c(
    c(
      "( mst ", " url ", " ccv ", " civ ", 
      " cip ", " oto ", " cit ", " nci ", 
      " sgo ", " pro ", " cig ", " cad ", 
      " cii ", " car ", " crc ", " der ", 
      " gas ", " nef ", " ort ", " mei ", 
      " hem ", " rad ", " pne ", " oft ", 
      " cib ", " onp )"
    ),
    collapse = "|"
  )
}
####
## Fim
#