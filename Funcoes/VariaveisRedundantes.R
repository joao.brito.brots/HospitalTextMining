#' Verificar se possui variaveis redundantes
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
VariaveisRedundantes <- function(dados){
 
  # Verifica se possui variaveis com conteudo constante
  variaveisRedundantes <- dados %>% 
    apply(
      MARGIN = 2L,
      FUN = function(coluna){
        all((coluna[1] == coluna), na.rm = TRUE)
      }
    ) %>% 
    any(na.rm = TRUE)
  
  # Retorno da funcao
  return(variaveisRedundantes) 
}
####
## Fim
#