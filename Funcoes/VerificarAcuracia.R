#' Verificacao da acuracia do modelo
#'
#' @param dadosTeste 
#' @param modelo 
#'
#' @return
#' @export
#'
#' @examples
VerificarAcuracia <- function(dadosTeste, modelo, classificador){
  
  # Gabarito
  gabarito <- dadosTeste$Classe
  
  # ClassificadorNeuralNetwork
  if(classificador == "ClassificadorNeuralNetwork"){
    predicao <- PredictNeuralNetwork(
      modelo = modelo,
      dadosTeste = dadosTeste
    )
    # ClassificadorKNN
  } else if(classificador == "ClassificadorKNN"){
    predicao <- PredictKNN(
      modelo = modelo,
      dadosTeste = dadosTeste
    )
    # ClassificadorGMDH
  } else if(classificador == "ClassificadorGMDH"){
    predicao <- PredictGMDH(
      modelo = modelo,
      dadosTeste = dadosTeste
    )
    # ClassificadorEnsembleGMDH
  } else if(classificador == "ClassificadorEnsembleGMDH"){
    predicao <- PredictGMDH(
      modelo = modelo,
      dadosTeste = dadosTeste
    )
  } else if(classificador == "ClassificadorXGBoost"){
    predicao <- PredictXGBoost(
      modelo = modelo,
      dadosTeste = dadosTeste
    )
  } else if(classificador == "ClassificadorRUSBoost"){
    predicao <- PredictRUSBoost(
      modelo = modelo,
      dadosTeste = dadosTeste
    )
  } else if(classificador == "ClassificadorSVM"){
    predicao <- PredictSVM(
      modelo = modelo,
      dadosTeste = dadosTeste
    )
  } else {
    predicao <- PredictGeral(
      modelo = modelo,
      dadosTeste = dadosTeste
    )
  } 
  
  # Se houve erro 
  if(Erro(predicao)){
    return(predicao)
  }
  
  # Cria matriz de confusao
  matrizConfusao <- try(
    expr = caret::confusionMatrix(
      predicao,
      gabarito,
      positive = "TRUE"
    ),
    silent = TRUE
  )  
  
  # Se houve erro 
  if(Erro(matrizConfusao)){
    return(matrizConfusao)
  }
  
  # AUC  
  matrizConfusao$AUC <- ModelMetrics::auc(
    actual = gabarito,
    predicted = predicao
  )
  
  # F1 Score
  matrizConfusao$FBetaScore <- MLmetrics::FBeta_Score(
    y_true = gabarito,
    y_pred = predicao,
    positive = "TRUE",
    beta = parSys$Geral$FBetaScore_Beta
  )
  
  # Se for acuracia
  if(parSys$SeletorVariaveis$CRV_Criterio == "Acuracia"){
    matrizConfusao$CRV <- matrizConfusao$overall[1]
    # Se for Sensibilidade
  } else if(parSys$SeletorVariaveis$CRV_Criterio == "Sensibilidade"){
    matrizConfusao$CRV <- matrizConfusao$byClass[1]
    # Se for Area Under the ROC Curve
  } else if(parSys$SeletorVariaveis$CRV_Criterio == "AUC"){
    matrizConfusao$CRV <- matrizConfusao$AUC
    # Se for F1Score
  } else if(parSys$SeletorVariaveis$CRV_Criterio == "FBetaScore"){
    matrizConfusao$CRV <- matrizConfusao$FBetaScore
  } 
  
  # Retorno da funcao
  return(matrizConfusao)
}
####
## Fim
#
