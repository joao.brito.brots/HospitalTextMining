---
title: "Best Tunning"
output: html_document
---

```{r results='hide', warning=FALSE, message=FALSE, error=FALSE, echo=FALSE}
#### Setup ####
source("_Setup.R")

modelos <- BuscarCache(
  arquivo = "Modelos"
)
```


```{r  results='asis'}
for(dias in names(modelos)){
  
  cat(
    str_c(
      "## ", dias, "\n"
    )
  )
  
  modelos[[dias]]$bestTune %>% 
    kable(
      format = "pipe",
      format.args	= list(
        big.mark = ".",
        decimal.mark = ","
      )
    ) %>% 
    cat(sep = "\n")
}
```

