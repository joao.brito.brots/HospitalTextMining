---
title: "Clusters"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
source("_Setup.R")
```

## Dados 

```{r message=TRUE, warning=TRUE, include=FALSE}
# Carrega dados originais
dadosOriginais <- ReestruturarDados(
  buscarCache = TRUE,
  dados = dadosOriginais
) %>% 
  filter(
    (TipoCAD_oft == 0),
    (TipoCAD_mst == 0),
    (TipoCAD_cib == 0),
    (TipoCAD_nef == 0),
    (TipoCAD_gas == 0),
    (TipoCAD_car == 0),
    (TipoCAD_rad == 0),
    (TipoCAD_mei == 0),
    (TipoCAD_hem == 0),
    (TipoCAD_onp == 0),
    (TipoCAD_crc == 0),
    (TipoCAD_der == 0)
  ) %>% 
  select(
    -TipoCAD_oft,
    -TipoCAD_mst,
    -TipoCAD_cib,
    -TipoCAD_nef,
    -TipoCAD_gas,
    -TipoCAD_car,
    -TipoCAD_rad,
    -TipoCAD_mei,
    -TipoCAD_hem,
    -TipoCAD_onp,
    -TipoCAD_crc,
    -TipoCAD_der
  ) %>% 
  filter(
    !is.na(DiasPermanencia),
    DiasPermanencia > 9
  ) %>% 
  select(
    DiasPermanencia, 
    IdadeAnos, Sexo_feminino, Sexo_masculino,
    starts_with("Asa"), 
    starts_with("Plano"),
    CirurgiaDiaMes, 
    starts_with("CirurgiaDiaSemana_"),
    starts_with("TipoAnestesiaGeral"), 
    starts_with("Carater"),
    Participantes,
    starts_with("TipoCAD")
  ) %>% 
  select(
    DiasPermanencia, Asa_ii, Asa_iii, Asa_iv, TipoAnestesiaGeral, 
    Plano_PlanoPrivado, Plano_PlanoSUS, TipoCAD_sgo, TipoCAD_civ, TipoCAD_cii
  )
```

## Analise numero clusters

```{r}
# Analise de clusters
analiseClusters <- NbClust(
  data = dadosOriginais %>% 
    select(-DiasPermanencia) %>% 
    na.omit() %>% 
    mutate_all(as.numeric) %>% 
    as.matrix(),
  distance = "euclidean",
  method = "kmeans",
  index = "dindex"
)
```

## Cria clusters

```{r}
clusters <- kmeans(
  x = dadosOriginais %>% 
    select(-DiasPermanencia) %>% 
    mutate_all(as.numeric) %>% 
    as.matrix(),
  centers = 3L
)
```

## Gráfico

```{r}
# Dados PCA
dadosPCA <- princomp(
  x = dadosOriginais %>% 
    select(-DiasPermanencia) %>% 
    mutate_all(as.numeric) %>% 
    as.matrix()
)

dadosPCA$scores %>% 
  as_tibble() %>% 
  distinct_all() %>% 
  nrow()

dadosPCA$scores %>% 
  as_tibble() %>% 
  select(Comp.1, Comp.2, Comp.3) %>% 
  mutate(
    Clusters = clusters$cluster
  ) %>% 
  plot_ly(
    x = ~Comp.1,
    y = ~Comp.2,
    z = ~Comp.3,
    color = ~Clusters,
    colors = c("blue", "green", "red"),
    size = 1
  )  
```




