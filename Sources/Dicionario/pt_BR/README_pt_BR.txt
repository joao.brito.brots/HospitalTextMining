Copyright (C) 2006 - 2007 por/by Raimundo Santos Moura <raimundomoura@openoffice.org>

=============
APRESENTAÇÃO
=============

O Projeto Verificador Ortográfico do BrOffice.org é um projeto 
colaborativo desenvolvido pela comunidade Brasileira. 
A relação completa dos colaboradores deste projeto está em: 
http://www.broffice.org.br/creditos

***********************************************************************
* Este é um dicionário para correção ortográfica da língua Portuguesa *
* para o Myspell.                                                     *
* Este programa é livre e pode ser redistribuído e/ou modificado nos  *
* termos da GNU Lesser General Public License (LGPL) versão 2.1.      *
*                                                                     *
***********************************************************************

======================
SOBRE ESTA ATUALIZAÇÃO
======================

Nova Estrutura de Afixos  

Os arquivos foram totalmente reestruturados visando torná-los mais
organizados e mais compreensíveis. Novas regras foram criadas e 
aplicadas em todo o léxico. As regras para prefixos, sufixos e verbos
receberam padronizações que vieram facilitar suas identificações,
gerando mais otimização dos arquivos.

O Trema 

Ponto de controvérsias quanto à sua usabilidade, este elemento é tema
de discussão entre os que defendem a sua abolição e os que desejam a
sua permanência embasados na legislação vigente.

Até a versão anterior as palavras com tremas eram validadas também sem
ele, a exemplo de 'freqüência' e 'frequência', como forma de tentar
agradar a gregos e troianos, uma vez que muitos veículos de comunicação
já aboliram a sua utilização.

A partir desta atualização, em atenção às solicitações dos nossos
colaboradores, decidimos rever a nossa posição e retirar as variações
sem o trema, respeitando o que está definido no sistema ortográfico
brasileiro aprovado pela Academia Brasileira de Letras em 1943, com as
alterações introduzidas pela reforma de 1971.

Salientamos que esta situação ocorria apenas na versão brasileira. A 
versão portuguesa desde o início fora concebida sem o uso do trema.

Verbos 

Foram corrigidas várias conjugações de verbos terminados em 'ear', 
'gar', e 'zer'.
Conjugações terminadas em 'írdes' (com acento) foram também corrigidas.
Mais verbos foram acrescentados nesta atualização.

Diminutivos  

Muitos diminutivos de palavras terminadas em 'são' e 'ção' foram 
endireitados. Outras palavras também sofreram correção em suas formas 
diminutivas.

Novos Termos Jurídicos  

Nesta atualização foram incluídos novos Termos Jurídicos do idioma 
português, uma colaboração de Roberto Carlos Martins Pires, que também 
é autor de um dicionário temático sobre o assunto retratando os termos 
em outros idiomas, usados pelo Direito Brasileiro, disponibilizado na 
página do Projeto Dicionários Temáticos.

Novas palavras  

O léxico conta ainda com inúmeras palavras novas, resultado da 
participação ativa dos nossos colaboradores. Houve, também, algumas 
alterações e exclusões de termos incorretos.

=======================================================
COMO INSTALAR O VERIFICADOR BRASILEIRO NO BROFFICE.ORG
=======================================================

Copie os arquivos pt_BR.dic e pt_BR.aff para o diretório <BrOffice.org>
/share/dict/ooo, onde <BrOffice.org> é o diretório em que o programa 
foi instalado.

No Windows, normalmente, o caminho é este: 
C:\Arquivos de programas\BrOffice.org 2.0\share\dict\ooo, e no  Linux
/opt/BrOffice.org/share/dict/ooo/.

No mesmo diretório, localize o arquivo dictionary.lst. Abra-o com um
editor de textos e acrescente a seguinte linha ao final(se não
existir):

DICT pt BR pt_BR

É necessário reiniciar o BrOffice, inclusive o início rápido da versão
para Windows que fica na barra de tarefas, para que o corretor
funcione.

===================
DÚVIDAS FREQÜENTES
===================

Os arquivos foram copiados mas o Verificador não está funcionando.
O Verificador Ortográfico não deve estar configurado corretamente,
isto pode estar ocorrendo por um dos seguintes motivos:

1- O dicionário provavelmente não está instalado.

Para se certificar de que está utilizando o idioma correto confira como
estão as informações em: Ferramentas >> Opções >>   Configurações de
Idioma >> Idiomas. O item Ocidental deve apresentar o dicionário
selecionado (deve aparecer um logo "Abc" do lado do idioma).

Se não estiver Português (Brasil) mude para esse idioma. Após
configurado clique em 'OK'.
Feche o BrOffice.org, inclusive o Iniciador Rápido,  e em seguida reabra-o;


2 - O verificador não está configurado para verificar texto ao digitar.
Neste caso confira como estão as informações em: Ferramentas >> Opções
>> Configurações de Idiomas >> Recursos de Verificação Ortográfica e, 
no campo opções deste formulário marque a opção 'Verificar texto ao 
digitar';


Novas atualizações estarão disponíveis no site do BrOffice.Org, na
página do Verificador Ortográfico.

http://www.broffice.org.br/verortografico



============
INTRODUCTION
============

The BrOffice.org Orthography Checker is a colaborative project developed
by the Brazilian community.
The complete list of participants in this project is at
http://www.broffice.org.br/creditos

***********************************************************************
* This is a dictionary for orthography correction for the Portuguese  *
* language for Myspell.                                               *
* This is a free program and it can be redistributed and/or           *
* modified under the terms of the GNU Lesser General Public License   *
* (LGPL) version 2.1.                                                 *
*                                                                     *
***********************************************************************

=================
ABOUT THIS UPDATE
=================

New affix structure

The files have been completely restructured with the purpose of letting
them more organized and understandable. New rules have been created and
applied in all the lexicon. The rules for prefixes, suffixes and verbs
have been standardized ir order to ease their identifications,
generating more optimized files.


The diaresis

A point of controversy as to its usability, this element is topic for discussion
among those that defend its abolition and those who desire its permanence, based
on current legislation.

As far as the previous version, words with diaresis were also validated
without it, in words such as 'freqüência' and 'frequência', as a way to
try to please both Greeks and Trojans, as many communication vehicles
have already abolished its use.

From this update onwards, answering the requests of our colaborators,
we have decided to review this stance and remove the variations without the
diaresis, respecting what is defined in the Brazilian ortographic system approved
by the Brazilian Academy of Letters in 1943, with the changes introduced by the
1971 reform.

We enphasize that this situation occurred only in the Brazilian version. The
Portuguse version, from its very start, was conceived without the use of the
diaresis.


Verbs

Many verb conjugations ending in 'ear', 'gar', and 'zer'. Conjugations
ending in 'írdes' (with accent) have also been corrected. More verbs
have been added in this update.


Diminutives

Many word diminutives ending in 'são' and 'ção' have been straightened
out. Other words have also been corrected in its diminutive forms.


New Legal Terms

In this update new Law Terms from the portuguese language have been
included, a colaboration by Roberto Carlos Martins Pires, who is also
the author of a theme dictionary on the subject, portraying the terms
in other languages, used by Brazilian law, available on the page of the
Thematic Dictionary Projects.


New words

The lexicon has now many new words, a result of the active participation
of our colaborators. There has been, in addition, a few changes and
exclusion of incorrect terms.


==============================================================
HOW TO INSTALL THE BRAZILIAN ORTOGRAPH CHECKER IN BROFFICE.ORG
==============================================================

Copy the files pt_BR.dic and pt_BR.aff to the directory <BrOffice.org>
/share/dict/ooo, where <BrOffice.org> is the directory where the software
has been installed.

In Windows, usually, the path is
C:\Arquivos de programas\BrOffice.org 2.0\share\dict\ooo, and in GNU/Linux
/opt/BrOffice.org/share/dict/ooo/.

In the same directory, locate the file dictionary.lst. Open it with a
text editor e add the following line to the end of the file (if it is
not already there):

DICT pt BR pt_BR

It is necessary to restart BrOffice, including the fast start for the Windows version
that resides on the task bar, in order to have the orthography checker to work.


==========================
FREQUENTLY ASKED QUESTIONS
==========================

The files have been copied but the checker is not working. The orthography checker may not be
configured correctly, this may be due to one of the following reasons:

1- The dictionary is probably not installed.

To make sure that you are using the right language, check the information at
Ferramentas >> Opções >>  Configurações de Idioma >> Idiomas.
The item "Ocidental" must present the selected dictionary (a logo "Abc" should
appear beside the language).
If the language selected is not "Português (Brasil)" change to this language.
After the configuration is correct, click on 'OK'.
Close BrOffice and the fast start, and open it afterwards;

2 - The checker is not configured to verify the orthography on typing. For this

problem, check the information at
"Ferramentas >> Opções >> Configurações de Idiomas >> Recursos de Verificação Ortográfica"
and, in the field "Opções" of this form, check the option ''Verificar texto ao digitar';

New updates will be available at the BrOffice.Org website, on the page of the
Orthography Checker.

http://www.broffice.org/verortografico